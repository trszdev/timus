#env python2
ndigit, bsum = tuple(map(int, raw_input().split()))

def binom(n, k):
    result = 1
    for i in xrange(1, k+1):
        result = result * (n-i+1) / i
    return result


def amount_of_lucky_tickets(ndigit, s):
    if s%2==1: return 0
    if s==0: return 1
    s/=2
    if s>=10**ndigit: return 0
    ndigit-=1
    a = binom(s+ndigit,ndigit)
    return a**2

print amount_of_lucky_tickets(ndigit, bsum)
