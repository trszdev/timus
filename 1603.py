#!env python2
erudit = ''.join(raw_input() for x in xrange(4))
n = input()

indeces = {}

a = map(lambda x: [x], xrange(16))

for i in xrange(1, 17):
    indeces[i] = a
    b = []
    for x in a:
        s = x[-1]
        sp1 = s+1
        sm1 = s-1
        sp4 = s+4
        sm4 = s-4
        if not sp1 in x and sp1%4!=0: b.append(x+[sp1])
        if not sm1 in x and s%4!=0: b.append(x+[sm1])
        if not sp4 in x and s<12: b.append(x+[sp4])
        if not sm4 in x and s>3: b.append(x+[sm4])
    a = b

def concat(ind): 
    r = ''
    for x in ind: r+=erudit[x]
    return r

possibles = {x:[concat(y) for y in indeces[x]] for x in xrange(1,17)}

def is_erudit(q):
    return q in possibles[len(q)]

for x in xrange(n):
    query = raw_input()
    is_ok = is_erudit(query)
    print "{0}: {1}".format(query, "YES" if is_ok else "NO")



