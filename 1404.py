#python2
num_to_letter = 'abcdefghijklmnopqrstuvwxyz'
letter_to_num = {num_to_letter[x]:x for x in xrange(len(num_to_letter))}
encrypted = raw_input()
first = letter_to_num[encrypted[0]]
decrypted = [first-5 if first >=5 else 21+first]

for x in encrypted[1:]:
    n = letter_to_num[x]
    a = first
    for y in xrange(first, first+26):
        if y % 26 == n: 
            first = y
            break
    decrypted.append(first - a)

print ''.join(num_to_letter[x] for x in decrypted)


