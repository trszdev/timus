#python2
n, m = map(int, raw_input().split())
amount = 5000
clones = [[] for _ in xrange(amount)]
heads, tails = [-1]*amount, [0]*amount
current = 1

for x in xrange(n): 
    cmd = raw_input().split()
    func = cmd[0]
    clone_number = int(cmd[1])
    if func=='learn': 
        # TODO
        programs = clones[clone_number]
        tail = tails[clone_number]+1
        if tail<0: 
            del programs[tail:]
        elif tail==0: del programs[-1]
        heads[clone_number] = len(programs)
        programs.append(cmd[2])
        tails[clone_number] = 0
    elif func=='rollback': 
        tails[clone_number] -= 1
    elif func=='relearn': 
        tails[clone_number] += 1
    elif func=='check': 
        programs = clones[clone_number]
        index = heads[clone_number]+tails[clone_number]
        print programs[index] if index>=0 else 'basic'
    elif func=='clone': 
        current+=1
        clones[current]=clones[clone_number][:]
        tails[current] = tails[clone_number]
        heads[current] = heads[clone_number]

