import math
parts = raw_input().split()
n, r = int(parts[0]), float(parts[1])
p = 2*math.pi*r
x2, y2 = 0,0
x1, y1 = (float(a) for a in raw_input().split())
xf, yf = x1, y1

def getdist():
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)

for i in xrange(n-1):
    x2, y2 = (float(a) for a in raw_input().split())
    p += getdist()
    x1, y1 = x2, y2

x2, y2 = xf, yf
p += getdist()

print round(p,2)

