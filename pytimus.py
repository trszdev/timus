#python2
import sys, glob, subprocess, time
from bs4 import BeautifulSoup

class Task:
    example_db_body = '''<testing>
    <test name="First example">
        <input>1</input>
        <output>1</output>
    </test>\n</testing>'''
    example_script_body = "print 'hello world'"


    def __init__(self, **args):
        self.name = args.get('name', 'unknown')
        self.script_name = self.name+'.py'
        self.db_name = self.name+'.xml'
    
    def _load_db(self):
        text = open(self.db_name, 'r')
        return BeautifulSoup(text, 'html.parser')

    def create(self):
        existing_files = self._get_available_task_files()
        if not self.script_name in existing_files:
            with open(self.script_name, 'w') as f:
                f.write(Task.example_script_body)
            print 'Created %s' % self.script_name
        if not self.db_name in existing_files:
            with open(self.db_name , 'w') as f:
                f.write(Task.example_db_body)
            print 'Created %s' % self.db_name

    def _get_available_task_files(self): 
        fnames = set([self.script_name, self.db_name])
        return fnames.intersection(glob.glob('*'))

    def _check_task_presence(self):
        necessary_files = set([self.script_name, self.db_name])
        assert self._get_available_task_files()==necessary_files

        
    def _get_script_output(self, inp):
        pipe = subprocess.PIPE
        proc = subprocess.Popen(["python", self.script_name], stdout=pipe, stdin=pipe)
        return proc.communicate(input=inp)[0].replace('\r','')[:-1]
        

    def test(self): 
        print 'Testing %s solution...' % self.name
        self._check_task_presence()
        tests = self._load_db().testing.find_all('test')
        total_tests = len(tests)
        if total_tests==0: 
            print 'There are no tests :)'
            return
        passed = True
        totaltime = 0
        for i in xrange(total_tests):
            test = tests[i]
            tin = str(test.input.text)
            tout = str(test.output.text)
            spendtime = -time.time()
            sout = self._get_script_output(tin)
            spendtime += time.time()
            totaltime += spendtime
            spendtime = round(int(spendtime * 1000))
            tpassed = sout==tout
            tpassedstr = 'passed' if tpassed else 'wrong'
            if not tpassed: passed = False
            tname = test.get('name')
            tnamestr = ' ('+tname+')' if tname else '' 
            print '\t[%d/%d] test%s: %s, %dms' % (i+1, total_tests, tnamestr, tpassedstr, spendtime)
        totaltime /= total_tests
        totaltime = round(int(totaltime * 1000))
        print 'Status: %s\nTime: %dms' % ('passed' if passed else 'failed', totaltime)

    def debug(self):
        print 'Debugging %s solution...' % self.name
        self._check_task_presence()
        tests = self._load_db().testing.find_all('test')
        total_tests = len(tests)
        if total_tests==0: 
            print 'There are no tests :)'
            return
        passed = True
        for i in xrange(total_tests):
            test = tests[i]
            tin = str(test.input.text)
            tout = str(test.output.text)
            sout = self._get_script_output(tin)
            if sout==tout: continue
            passed = False
            tname = test.get('name')
            tnamestr = ' ('+tname+')' if tname else '' 
            print '[%d/%d] test%s:' % (i+1, total_tests, tnamestr)
            tstrs = tout.split('\n')
            sstrs = sout.split('\n')
            diff = len(tstrs)-len(sstrs)
            if diff>0:
                sstrs.extend(['']*diff)
            elif diff<0:
                tstrs.extend(['']*(-diff))
            print '\t%s | %s\n\t%s' % ('>>expected<<'.ljust(20), '>>got<<', '-'*40)
            for x in xrange(len(tstrs)): 
                print '\t%s | %s' % (tstrs[x].ljust(20),sstrs[x])
            
        print 'Status: %s' % ('passed' if passed else 'failed')
    


def print_help(): 
    print '\tusage: python pytimus.py [create|test|debug] [task]\n'
    print '\t-> create - make example script and testing files'
    print '\t-> test - pass all tests with stopwatch'
    print '\t-> debug - linebyline comparison with expected output'

def main(args):
    if len(args)!=2:
        print_help()
        return 1
    cmd = args[0]
    task = Task(name=args[1])
    methods = {'create': task.create, 'test': task.test, 'debug': task.debug}
    if not cmd in methods: 
        print_help()
        return 2
    methods[cmd]()
    return 0

if __name__=='__main__':
    main(sys.argv[1:])
